/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqueteImplement;

import java.util.List;
import modelo.Puesto;
import org.hibernate.Session;
import org.hibernate.Transaction;
import paquteInter.puestoInter;
import util.HibernateUtil;

/**
 *
 * @author JUANCHO
 */
public class puestoImple implements puestoInter{

    @Override
    public List<Puesto> listarPuesto() {
        List<Puesto> lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction a = session.beginTransaction();
        String hql = "FROM Puesto";
        try {
            lista = session.createQuery(hql).list();
            a.commit();
            session.close();
        } catch (Exception e) {
            a.rollback();
        }
        return lista;
    }

    @Override
    public void nuevoPuesto(Puesto puesto) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(puesto);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public void actualizarPuesto(Puesto puesto) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(puesto);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public void eliminarPuesto(Puesto puesto) {
         Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(puesto);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
    
}
