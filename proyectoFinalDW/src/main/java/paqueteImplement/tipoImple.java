/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqueteImplement;

import java.util.List;
import modelo.Tipo;
import org.hibernate.Session;
import org.hibernate.Transaction;
import paquteInter.tipoInter;
import util.HibernateUtil;

/**
 *
 * @author JUANCHO
 */
public class tipoImple implements tipoInter {

    @Override
    public List<Tipo> listarTipos() {
        List<Tipo> lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction a = session.beginTransaction();
        String hql = "FROM Tipo";
        try {
            lista = session.createQuery(hql).list();
            a.commit();
            session.close();
        } catch (Exception e) {
            a.rollback();
        }
        return lista;
    }

    @Override
    public void newTipo(Tipo tipo) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(tipo);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public void updateTipo(Tipo tipo) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(tipo);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public void deleteTipo(Tipo tipo) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(tipo);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

}
