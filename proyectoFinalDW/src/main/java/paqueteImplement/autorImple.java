/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqueteImplement;

import java.util.List;
import modelo.Autor;
import org.hibernate.Session;
import org.hibernate.Transaction;
import paquteInter.autorInter;
import util.HibernateUtil;

/**
 *
 * @author JUANCHO
 */
public class autorImple implements autorInter {

    @Override
    public List<Autor> listarAutor() {
        List<Autor> lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction a = session.beginTransaction();
        String hql = "FROM Autor";
        try {
            lista = session.createQuery(hql).list();
            a.commit();
            session.close();
        } catch (Exception e) {
            a.rollback();
        }
        return lista;
    }

    @Override
    public void newAutor(Autor autor) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(autor);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public void updateAutor(Autor autor) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(autor);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }

    }

    @Override
    public void deleteAutor(Autor autor) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(autor);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

}
