/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqueteImplement;

import java.util.List;
import modelo.Libro;
import org.hibernate.Session;
import org.hibernate.Transaction;
import paquteInter.libroInter;
import util.HibernateUtil;

/**
 *
 * @author JUANCHO
 */
public class libroImple implements libroInter{

    @Override
    public List<Libro> listarLibro() {
        List<Libro> lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction a = session.beginTransaction();
        String hql = "FROM Libro";
        try {
            lista = session.createQuery(hql).list();
            a.commit();
            session.close();
        } catch (Exception e) {
            a.rollback();
        }
        return lista;
    }

    @Override
    public void nuevoLibro(Libro libro) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(libro);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public void actualizarLibro(Libro libro) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(libro);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public void eliminarLibro(Libro libro) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(libro);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
 
}
