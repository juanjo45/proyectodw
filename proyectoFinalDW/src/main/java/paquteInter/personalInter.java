/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquteInter;

import java.util.List;
import modelo.Personal;

/**
 *
 * @author JUANCHO
 */
public interface personalInter {
    
    public List<Personal> listarPersonal();

    public void nuevoPersonal(Personal personal);

    public void actualizarPersonal(Personal personal);

    public void eliminarPersonal(Personal personal);
    
}
