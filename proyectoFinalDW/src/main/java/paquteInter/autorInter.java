/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquteInter;

import java.util.List;
import modelo.Autor;

/**
 *
 * @author JUANCHO
 */
public interface autorInter {
    
    public List<Autor> listarAutor();

    public void newAutor(Autor autor);

    public void updateAutor(Autor autor);

    public void deleteAutor(Autor autor);
    
}
