/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquteInter;

import java.util.List;
import modelo.Tipo;

/**
 *
 * @author JUANCHO
 */
public interface tipoInter {
    
    public List<Tipo> listarTipos();

    public void newTipo(Tipo tipo);

    public void updateTipo(Tipo tipo);

    public void deleteTipo(Tipo tipo);
    
}
