/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquteInter;

import java.util.List;
import modelo.Libro;

/**
 *
 * @author JUANCHO
 */
public interface libroInter {
    
    public List<Libro> listarLibro();

    public void nuevoLibro(Libro libro);

    public void actualizarLibro(Libro libro);

    public void eliminarLibro(Libro libro);
    
}
