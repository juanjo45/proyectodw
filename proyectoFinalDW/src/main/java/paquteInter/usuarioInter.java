/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquteInter;

import java.util.List;
import modelo.Usuario;

/**
 *
 * @author JUANCHO
 */
public interface usuarioInter {
    
        public List<Usuario> listarUsuario();

    public void nuevoUsuario(Usuario usuario);

    public void actualizarUsuario(Usuario usuario);

    public void eliminarUsuario(Usuario usuario);
    
}
