/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquteInter;

import java.util.List;
import modelo.Control;

/**
 *
 * @author JUANCHO
 */
public interface controlInter {

    public List<Control> listarControl();

    public void nuevoControl(Control control);

    public void actualizarControl(Control control);

    public void eliminarControl(Control control);

}
