/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquteInter;

import java.util.List;
import modelo.Puesto;

/**
 *
 * @author JUANCHO
 */
public interface puestoInter {
    
    public List<Puesto> listarPuesto();

    public void nuevoPuesto(Puesto puesto);

    public void actualizarPuesto(Puesto puesto);

    public void eliminarPuesto(Puesto puesto);
    
}
