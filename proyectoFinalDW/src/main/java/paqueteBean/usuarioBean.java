/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqueteBean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import modelo.Usuario;
import org.primefaces.event.SelectEvent;
import paqueteImplement.usuarioImple;
import paquteInter.usuarioInter;

/**
 *
 * @author JUANCHO
 */
@ManagedBean
@ViewScoped
public class usuarioBean implements Serializable {

    private List<Usuario> listarUsuario;
    private Usuario usuario;
    private List<SelectItem> seleccionarUsuario;
    private String f_seleccionada = "";

    /**
     * Creates a new instance of usuarioBean
     */
    public usuarioBean() {
        if (usuario == null) {
            usuario = new Usuario();
        }
    }

    public List<Usuario> getListarUsuario() {
        usuarioInter dato = new usuarioImple();
        listarUsuario = dato.listarUsuario();
        return listarUsuario;
    }

    public void setListarUsuario(List<Usuario> listarUsuario) {
        this.listarUsuario = listarUsuario;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<SelectItem> getSeleccionarUsuario() {
        this.seleccionarUsuario = new ArrayList<SelectItem>();
        usuarioInter id = new usuarioImple();

        List<Usuario> list = id.listarUsuario();
        seleccionarUsuario.clear();

        for (Usuario dir : list) {
            SelectItem lista = new SelectItem(dir.getIdusuario(), dir.getNombre().concat(' '+dir.getApellido()));
            this.seleccionarUsuario.add(lista);
        }
        return seleccionarUsuario;
    }

    public void setSeleccionarUsuario(List<SelectItem> seleccionarUsuario) {
        this.seleccionarUsuario = seleccionarUsuario;
    }
    
    public void prepararNuevoUsuario() {
        if (usuario == null) {
            usuario = new Usuario();
        }
    }

    /**
     * Metodo para guardar un objeto
     */
    public void nuevoUsuario() {
        usuarioInter nuevo = new usuarioImple();
        nuevo.nuevoUsuario(usuario);
        usuario = new Usuario();
    }

    /**
     * Metodo para modificar un objeto
     */
    public void modificarUsuario() {
        usuarioInter actualizar = new usuarioImple();
        actualizar.actualizarUsuario(usuario);
        usuario = new Usuario();
    }

    /**
     * Metodo para eliminar un objeto
     */
    public void eliminarUsuario() {
        usuarioInter eleiminar = new usuarioImple();
        eleiminar.eliminarUsuario(usuario);
        usuario = new Usuario();
    }

    public String getF_seleccionada() {
        return f_seleccionada;
    }

    public void setF_seleccionada(String f_seleccionada) {
        this.f_seleccionada = f_seleccionada;
    }

    public void actualizar_fecha(SelectEvent event) {
        SimpleDateFormat fecha1 = new SimpleDateFormat("EEEEE dd MMMMM yyyy");
        StringBuilder cadena_fecha1_11 = new StringBuilder(fecha1.format(event.getObject()));
        f_seleccionada = cadena_fecha1_11.toString();

    }

}
