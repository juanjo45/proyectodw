/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqueteBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import modelo.Autor;
import paqueteImplement.autorImple;
import paquteInter.autorInter;

/**
 *
 * @author JUANCHO
 */
@ManagedBean
@ViewScoped
public class autorBean implements Serializable{

    private List<Autor> listarAutor;
    private Autor autor;
    private List<SelectItem> seleccionarAutor;

    /**
     * Creates a new instance of autorBean
     */
    public autorBean() {
        if (autor == null) {
            autor = new Autor();
        }

    }

    public List<Autor> getListarAutor() {
        autorInter dato = new autorImple();
        listarAutor = dato.listarAutor();
        return listarAutor;
    }

    public void setListarAutor(List<Autor> listarAutor) {
        this.listarAutor = listarAutor;
    }

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    public List<SelectItem> getSeleccionarAutor() {
        this.seleccionarAutor = new ArrayList<SelectItem>();
        autorInter id = new autorImple();

        List<Autor> list = id.listarAutor();
        seleccionarAutor.clear();

        for (Autor dir : list) {
            SelectItem lista = new SelectItem(dir.getIdautor(), dir.getNombreautor());
            this.seleccionarAutor.add(lista);
        }

        return seleccionarAutor;
    }

    public void setSeleccionarAutor(List<SelectItem> seleccionarAutor) {
        this.seleccionarAutor = seleccionarAutor;
    }

    public void prepararNuevaAutor() {
        if (autor == null) {
            autor = new Autor();
        }
    }

    /**
     * Metodo para guardar un objeto
     */
    public void nuevoaAutor() {
        autorInter nuevo = new autorImple();
        nuevo.newAutor(autor);
        autor = new Autor();
    }

    /**
     * Metodo para modificar un objeto
     */
    public void modificarAutor() {
        autorInter actualizar = new autorImple();
        actualizar.updateAutor(autor);
        autor = new Autor();
    }

    /**
     * Metodo para eliminar un objeto
     */
    public void eliminarAutor() {
        autorInter eleiminar = new autorImple();
        eleiminar.deleteAutor(autor);
        autor = new Autor();
    }

}
