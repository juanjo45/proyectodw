/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqueteBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import modelo.Tipo;
import paqueteImplement.tipoImple;
import paquteInter.tipoInter;

/**
 *
 * @author JUANCHO
 */
@ManagedBean
@ViewScoped
public class tipoBean implements Serializable{
    
    private List<Tipo> listarTipos;
    private Tipo tipo;
    private List<SelectItem> seleccionarTipos;

    /**
     * Creates a new instance of tipoBean
     */
    public tipoBean() {
        if(tipo == null){
           tipo = new Tipo();
        }
    }

    public List<Tipo> getListarTipos() {
        tipoInter dato = new tipoImple();
        listarTipos = dato.listarTipos();
        return listarTipos;
    }

    public void setListarTipos(List<Tipo> listarTipos) {
        this.listarTipos = listarTipos;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public List<SelectItem> getSeleccionarTipos() {
        this.seleccionarTipos = new ArrayList<SelectItem>();
        tipoInter id = new tipoImple();

        List<Tipo> list = id.listarTipos();
        seleccionarTipos.clear();

        for (Tipo dir : list) {
            SelectItem lista = new SelectItem(dir.getIdtipo(), dir.getCategoria());
            this.seleccionarTipos.add(lista);
        }
        
        return seleccionarTipos;
    }

    public void setSeleccionarTipos(List<SelectItem> seleccionarTipos) {
        this.seleccionarTipos = seleccionarTipos;
    }
    
    public void prepararNuevaTipo() {
        if (tipo == null) {
            tipo = new Tipo();
        }
    }

    /**
     * Metodo para guardar un objeto
     */
    public void nuevoTipo() {
        tipoInter nuevo = new tipoImple();
        nuevo.newTipo(tipo);
        tipo = new Tipo();
    }

    /**
     * Metodo para modificar un objeto
     */
    public void modificarTipo() {
        tipoInter actualizar = new tipoImple();
        actualizar.updateTipo(tipo);
        tipo = new Tipo();
    }

    /**
     * Metodo para eliminar un objeto
     */
    public void eliminarTipo() {
        tipoInter eliminar = new tipoImple();
        eliminar.deleteTipo(tipo);
        tipo = new Tipo();
    }
    
}
