/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqueteBean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import modelo.Control;
import org.primefaces.event.SelectEvent;
import paqueteImplement.controlImple;
import paquteInter.controlInter;

/**
 *
 * @author JUANCHO
 */
@ManagedBean
@ViewScoped
public class controlBean implements Serializable{

    private List<Control> listarControl;
    private Control control;
    private String f_seleccionada = "";

    /**
     * Creates a new instance of controlBean
     */
    public controlBean() {
        if (control == null) {
            control = new Control();
        }
    }

    public List<Control> getListarControl() {
        controlInter dato = new controlImple();
        listarControl = dato.listarControl();
        return listarControl;
    }

    public void setListarControl(List<Control> listarControl) {
        this.listarControl = listarControl;
    }

    public Control getControl() {
        return control;
    }

    public void setControl(Control control) {
        this.control = control;
    }

     public void prepararNuevoControl() {
        if (control == null) {
            control = new Control();
        }
    }

    /**
     * Metodo para guardar un objeto
     */
    public void nuevoControl() {
        controlInter nuevo = new controlImple();
        nuevo.nuevoControl(control);
        control = new Control();
    }

    /**
     * Metodo para modificar un objeto
     */
    public void modificarControl() {
        controlInter actualizar = new controlImple();
        actualizar.actualizarControl(control);
        control = new Control();
    }

    /**
     * Metodo para eliminar un objeto
     */
    public void eliminarControl() {
        controlInter eleiminar = new controlImple();
        eleiminar.eliminarControl(control);
        control = new Control();
    }

    public String getF_seleccionada() {
        return f_seleccionada;
    }

    public void setF_seleccionada(String f_seleccionada) {
        this.f_seleccionada = f_seleccionada;
    }

    public void actualizar_fecha(SelectEvent event) {
        SimpleDateFormat fecha1 = new SimpleDateFormat("EEEEE dd MMMMM yyyy");
        StringBuilder cadena_fecha1_11 = new StringBuilder(fecha1.format(event.getObject()));
        f_seleccionada = cadena_fecha1_11.toString();
    }
    
}
