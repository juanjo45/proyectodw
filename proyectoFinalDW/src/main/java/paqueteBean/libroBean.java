/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqueteBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import modelo.Libro;
import paqueteImplement.libroImple;
import paquteInter.libroInter;

/**
 *
 * @author JUANCHO
 */
@ManagedBean
@ViewScoped
public class libroBean implements Serializable{

    private List<Libro> listarLibros;
    private Libro libro;
    private List<SelectItem> seleccionarLibro;
    
    /**
     * Creates a new instance of libroBean
     */
    public libroBean() {
        if (libro == null) {
            libro = new Libro();
        }
    }

    public List<Libro> getListarLibros() {
        libroInter dato = new libroImple();
        listarLibros = dato.listarLibro();
        return listarLibros;
    }

    public void setListarLibros(List<Libro> listarLibros) {
        this.listarLibros = listarLibros;
    }

    public Libro getLibro() {
        return libro;
    }

    public void setLibro(Libro libro) {
        this.libro = libro;
    }

    public List<SelectItem> getSeleccionarLibro() {
        this.seleccionarLibro = new ArrayList<SelectItem>();
        libroInter id = new libroImple();

        List<Libro> list = id.listarLibro();
        seleccionarLibro.clear();

        for (Libro dir : list) {
            SelectItem lista = new SelectItem(dir.getIdlibro(), dir.getNombrelibro());
            this.seleccionarLibro.add(lista);
        }
        
        return seleccionarLibro;
    }

    public void setSeleccionarLibro(List<SelectItem> seleccionarLibro) {
        this.seleccionarLibro = seleccionarLibro;
    }
    
    public void prepararNuevoLibro() {
        if (libro == null) {
            libro = new Libro();
        }
    }

    /**
     * Metodo para guardar un objeto
     */
    public void nuevoLibro() {
        libroInter nuevo = new libroImple();
        nuevo.nuevoLibro(libro);
        libro = new Libro();
    }

    /**
     * Metodo para modificar un objeto
     */
    public void modificarLibro() {
        libroInter actualizar = new libroImple();
        actualizar.actualizarLibro(libro);
        libro = new Libro();
    }

    /**
     * Metodo para eliminar un objeto
     */
    public void eliminarLibro() {
        libroInter eleiminar = new libroImple();
        eleiminar.eliminarLibro(libro);
        libro = new Libro();
    }
    
}
