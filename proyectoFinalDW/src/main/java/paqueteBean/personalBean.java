/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqueteBean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import modelo.Personal;
import org.primefaces.event.SelectEvent;
import paqueteImplement.personalImple;
import paquteInter.personalInter;

/**
 *
 * @author JUANCHO
 */
@ManagedBean
@ViewScoped
public class personalBean implements Serializable {

    private List<Personal> listarPersonal;
    private Personal personal;
    private List<SelectItem> seleccionarPersonal;
    private String f_seleccionada = "";

    /**
     * Creates a new instance of personalBean
     */
    public personalBean() {
        if (personal == null) {
            personal = new Personal();
        }
    }

    public List<Personal> getListarPersonal() {
        personalInter dato = new personalImple();
        listarPersonal = dato.listarPersonal();
        return listarPersonal;
    }

    public void setListarPersonal(List<Personal> listarPersonal) {
        this.listarPersonal = listarPersonal;
    }

    public Personal getPersonal() {
        return personal;
    }

    public void setPersonal(Personal personal) {
        this.personal = personal;
    }

    public List<SelectItem> getSeleccionarPersonal() {
        this.seleccionarPersonal = new ArrayList<SelectItem>();
        personalInter id = new personalImple();

        List<Personal> list = id.listarPersonal();
        seleccionarPersonal.clear();

        for (Personal dir : list) {
            SelectItem lista = new SelectItem(dir.getIdpuesto(), dir.getNombrepersona().concat(' '+dir.getApellido()));
            this.seleccionarPersonal.add(lista);
        }
        return seleccionarPersonal;
    }

    public void setSeleccionarPersonal(List<SelectItem> seleccionarPersonal) {
        this.seleccionarPersonal = seleccionarPersonal;
    }

    public void prepararNuevoPersonal() {
        if (personal == null) {
            personal = new Personal();
        }
    }

    /**
     * Metodo para guardar un objeto
     */
    public void nuevoPersonal() {
        personalInter nuevo = new personalImple();
        nuevo.nuevoPersonal(personal);
        personal = new Personal();
    }

    /**
     * Metodo para modificar un objeto
     */
    public void modificarPersonal() {
        personalInter actualizar = new personalImple();
        actualizar.actualizarPersonal(personal);
        personal = new Personal();
    }

    /**
     * Metodo para eliminar un objeto
     */
    public void eliminarPersonal() {
        personalInter eleiminar = new personalImple();
        eleiminar.eliminarPersonal(personal);
        personal = new Personal();
    }

    public String getF_seleccionada() {
        return f_seleccionada;
    }

    public void setF_seleccionada(String f_seleccionada) {
        this.f_seleccionada = f_seleccionada;
    }

    public void actualizar_fecha(SelectEvent event) {
        SimpleDateFormat fecha1 = new SimpleDateFormat("EEEEE dd MMMMM yyyy");
        StringBuilder cadena_fecha1_11 = new StringBuilder(fecha1.format(event.getObject()));
        f_seleccionada = cadena_fecha1_11.toString();

    }

}
