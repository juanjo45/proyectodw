/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqueteBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import modelo.Puesto;
import paqueteImplement.puestoImple;
import paquteInter.puestoInter;

/**
 *
 * @author JUANCHO
 */
@ManagedBean
@ViewScoped
public class puestoBean implements Serializable{

    private List<Puesto> listarPuesto;
    private Puesto puesto;
    private List<SelectItem> seleccionarPuesto;

    /**
     * Creates a new instance of puestoBean
     */
    public puestoBean() {
        if (puesto == null) {
            puesto = new Puesto();
        }
    }

    public List<Puesto> getListarPuesto() {
        puestoInter dato = new puestoImple();
        listarPuesto = dato.listarPuesto();
        return listarPuesto;
    }

    public void setListarPuesto(List<Puesto> listarPuesto) {
        this.listarPuesto = listarPuesto;
    }

    public Puesto getPuesto() {
        return puesto;
    }

    public void setPuesto(Puesto puesto) {
        this.puesto = puesto;
    }

    public List<SelectItem> getSeleccionarPuesto() {
        this.seleccionarPuesto = new ArrayList<SelectItem>();
        puestoInter id = new puestoImple();

        List<Puesto> list = id.listarPuesto();
        seleccionarPuesto.clear();

        for (Puesto dir : list) {
            SelectItem lista = new SelectItem(dir.getIdpuesto(), dir.getNombrepuesto());
            this.seleccionarPuesto.add(lista);
        }
        return seleccionarPuesto;
    }

    public void setSeleccionarPuesto(List<SelectItem> seleccionarPuesto) {
        this.seleccionarPuesto = seleccionarPuesto;
    }
    
    public void prepararNuevoPuesto() {
        if (puesto == null) {
            puesto = new Puesto();
        }
    }

    /**
     * Metodo para guardar un objeto
     */
    public void nuevoPuesto() {
        puestoInter nuevo = new puestoImple();
        nuevo.nuevoPuesto(puesto);
        puesto = new Puesto();
    }

    /**
     * Metodo para modificar un objeto
     */
    public void modificarPuesto() {
        puestoInter actualizar = new puestoImple();
        actualizar.actualizarPuesto(puesto);
        puesto = new Puesto();
    }

    /**
     * Metodo para eliminar un objeto
     */
    public void eliminarPuesto() {
        puestoInter eleiminar = new puestoImple();
        eleiminar.eliminarPuesto(puesto);
        puesto = new Puesto();
    }

}
